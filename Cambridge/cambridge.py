from bs4 import BeautifulSoup
import requests

url = 'https://www.cambridge.org/core/publications/journals'
response = requests.get(url)
soup = BeautifulSoup(response.content,'lxml')
#print(soup)
vol_list = []
class cambridge:
    
    def journals(self):
        journ_list = []

        with open('journal.txt','w') as file:
            base_url = 'https://www.cambridge.org/core/publications/journals'
            for links in soup.find_all('a',attrs={'class':'title'}):
                #print("href",links)
                href = links['href']
                urls = ("https://www.cambridge.org"+href)
                file.write(urls+'\n')
                journ_list.append(urls)
            print("journals",journ_list)


    def volume(self):
        
        with open('volume.txt','w') as f1:

            journ_url = 'https://www.cambridge.org/core/journals/britannia/all-issues'
            vol_res = requests.get(journ_url)
            
            vol_soup = BeautifulSoup(vol_res.content,'lxml')
            #print(vol_soup)
           
            for vol_link in vol_soup.find_all('a',attrs={'class':'row'}):
                #print(vol_link)
                vol_href = vol_link['href']
                #print(vol_href)
                
                vol_urls = ("https://www.cambridge.org"+vol_href)
                if '#' not in vol_urls:
                    #print(vol_urls)
                    f1.write(vol_urls+"\n")
                    vol_list.append(vol_urls)
                
                else:
                    pass
                
            print(vol_list)
    def article(self):
        #print(vol_list)
            
        with open('article.txt','w') as f2:
            art_list = []

            for art_url in vol_list:
                art_res = requests.get(art_url)
                art_soup = BeautifulSoup(art_res.content,'lxml')
                #print(art_soup)

                for art_link in art_soup.find_all('a',attrs={'class':'part-link'}):
                    #print(art_link)
                    art_href = art_link['href']
                    #print(art_href)

                    art_urls = ("https://www.cambridge.org"+art_href)
                    #print(art_urls) 

                    f2.write(art_urls+'\n')  
                    art_list.append(art_urls)
                #print(art_list)
   
datas = cambridge()
#datas.journals()   
datas.volume()
datas.article()


    

        
     
            

            





