from django.urls import path
from bapp import views

urlpatterns = [
   
    path('',views.homepage,name="homepage"),
    path('register',views.register,name="register"),
]