from django import forms

class CollegeForm(forms.Form):
    SUBJECT_CHOICES = (
        (1,'Electronics and Communication'),
        (2,'Computer Science'),
        (3,'Mechanical'),
        (4,'Civil'),
    )

    name = forms.CharField()
    age = forms.IntegerField()
    dept = forms.ChoiceField(choices=SUBJECT_CHOICES,
                             widget=forms.RadioSelect()
                            )
    date_of_birth = forms.DateField(widget=forms.DateInput(attrs={type:'date'}))
