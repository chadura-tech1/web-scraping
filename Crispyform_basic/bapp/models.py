from django.db import models



# Create your models here.
class Employee(models.Model):
    firstname = models.CharField(max_length=10,verbose_name='First Name')
    lastname = models.CharField(max_length=20)
    emailid = models.EmailField(verbose_name="Email ID")
    contact = models.IntegerField()
    address = models.TextField(blank=True,null=True)
    active = models.BooleanField()
    url = models.URLField(default=0)

    def __str__(self):  #object to string
        return f'Created by {self.firstname}' #f string+variable in python

class TestUser(models.Model):
    username1 = models.CharField(max_length=20,verbose_name="USERNAME")
    password1 = models.CharField(max_length=20,verbose_name="PASSWORD")
    email1   = models.EmailField()

    def __str__(self):  #object to string
        return f'Created by {self.username1}' 
