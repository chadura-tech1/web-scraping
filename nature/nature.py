from bs4 import BeautifulSoup
import requests

def scrap_links(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.content,'lxml')
    
    page_links = []
    with open('nature.txt','w') as file :
        num_list = []
        base_url ='https://www.nature.com/hdy/articles?searchType=journalSearch&sort=PubDate&year=2023&page='

        for page_element in soup.find_all('a',attrs={'class':'c-pagination__link'}):
           # print(page_element)
            href = page_element['href']
            #print(href)
            split = href.split("page=")
            #print(split)
            num_list.append(split[1])
        #print(num_list)
        lastElement = num_list[2]
        print(lastElement)
        page_range = range(1,int(lastElement)+1)
        #print(page_range)
        for page in page_range  :
            urls = base_url + str(page)
            #print("urls", urls)
            file.write((urls+"\n"))
            page_links.append(urls)
        print(page_links)

    with open('article.txt','w') as fp:

        for link in page_links:
            article_response = requests.get(link)
            article_soup = BeautifulSoup(article_response.content, 'lxml')
            for  article_link in article_soup.find_all('a',attrs={'class':'c-card__link u-link-inherit'}):
                articles = article_link['href']
                print("https://www.nature.com"+articles)
                fp.write(("https://www.nature.com"+articles)+"\n")
scrap_links('https://www.nature.com/hdy/articles?searchType=journalSearch&sort=PubDate&year=2023')





