
from django.urls import path
from bapp import views

urlpatterns = [
    path('',views.homepage,name="homepage"),
    path('login',views.loginpage,name="login"),
    path('register',views.registerpage,name="registerpage"),
    
    
]