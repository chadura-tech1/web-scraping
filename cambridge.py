from bs4 import BeautifulSoup
import requests

url = 'https://www.cambridge.org/core/publications/journals'
response = requests.get(url)
soup = BeautifulSoup(response.content,'lxml')
#print(soup)

journ_list = []

with open('journal.txt','w') as file:
    base_url = 'https://www.cambridge.org/core/publications/journals'
    for links in soup.find_all('a',attrs={'class':'title'}):
        #print("href",links)
        href = links['href']
        urls = ("https://www.cambridge.org"+href)
        file.write(urls+'/n')
        journ_list.append(urls)
    #print(journ_list)

    for volume in journ_list:
        vol_response = requests.get(volume)
        vol_soup = BeautifulSoup(vol_response.content,'lxml')
        #print(vol_soup)
        for vol_href in vol_soup.find('a',attrs={'class':'row'}):
            print(vol_href)
            vol_links = vol_href['href']
            #print(vol_links)
            break
    

