from bs4 import BeautifulSoup
import requests

response = requests.get("https://www.hindawi.com/journals/cggr/contents/")
soup = BeautifulSoup(response.text, 'lxml')
# print(soup)
urls=[]
with open('hindawi.txt','w') as f:
    base_url = "https://www.hindawi.com/journals/cggr/contents/page/"
    href = soup.find('a', class_= "sc-htpNat bUhGXt link sc-iELTvK cqojQs pagination__last")['href']
    #last_no = href[-3:-1]
    page_split = href.split('/')
    #print(page_split)
    last_num = page_split[-2]
    for page in range(1,int(last_num)+1):
        page_links = base_url+str(page)
        # print(page_links)
        f.write(page_links+'\n')
        urls.append(page_links)
    print(urls)
    for art_link in urls:
        art_response = requests.get(art_link)
        art_soup = BeautifulSoup(art_response.text, 'lxml')
        for art_href in art_soup.find_all('a',class_="sc-htpNat bUhGXt link sc-cEvuZC bTvMeW"):
            links = art_href['href']
            print("https://www.hindawi.com"+links)
            f1 = open('hindawi_article.txt','a')
            f1.write(("https://www.hindawi.com"+links)+'\n')